from django.urls import path , include
from django.http import HttpResponse
from . import views
urlpatterns = [
        path('', views.main , name='main'),
        path('login' , views.login_view , name='login'),
        path('newuser', views.new_user_view , name='newuser'),
        path('logout', views.logout_view , name='logout'),
        path('profile' , views.profile , name='profile'),
        path('events/add' , views.add_new , name='add_event'),
        path('events/<uuid:pk>' , views.detail , name= 'detail'),
        path('events/remove/<uuid:pk>', views.remove , name='remove'),
        path('events/edit/<uuid:pk>',views.edit ,name='edit'),
    #    path('events/<uuid:pk>/edit/' , views.detail_edit_view , name='edit'), 
        ]

from django.db import models
import uuid
from django.contrib.auth.models import User
from datetime import datetime
# Create your models here.


class Frage(models.Model):
    """
    something about project
    """
    # name of event
    name = models.CharField(max_length=100 , help_text="Name of the event")
    # place of event 
    platz  = models.CharField(max_length=200,help_text="Address of the event")
    # gets a string in json or python dict format
    opts = models.TextField(help_text="Insert the Options in JSON form")
    # we use uuid to generate urls
    id =  models.UUIDField(primary_key=True , default=uuid.uuid4 )
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    mod_date = models.DateField(default=datetime.today)

    class Meta:
        ordering = ["-mod_date"]
    
    def __str__(self):
        return self.name + " - " + self.platz 

    def get_absolute_url(self):
        """
        Returns the url to access a detail record for this book.
        """
        return reverse('frage-detail', args=[str(self.id)])
